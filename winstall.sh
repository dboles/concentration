#!/bin/bash

# FIXME: This doesn't work on Windows/MSYS2/MinGW64. Try to rewrite in Python...
#        https://github.com/mesonbuild/meson/issues/8565#issuecomment-1011129673
#        ...with the help of lddcollect:
#        https://pypi.org/project/lddcollect/

exe="$1"
prefix="$MESON_INSTALL_PREFIX"

cd "$MESON_SOURCE_ROOT"

./winstall_ldds.sh "$exe" "$prefix"

while IFS='' read -r line
do
	file="$MINGW_PREFIX/bin/$line"

	cp -Lp "$file" "$prefix"
	./winstall_ldds.sh "$file" "$prefix"
done < winstall_bins.txt

while IFS=$'\t' read -r dirs1 dir2
do
	dir="$MINGW_PREFIX/$dirs1/$dir2"
	dst="$prefix/$dirs1"

	mkdir -p "$dst"
	cp -Lpr "$dir" "$dst"
done < winstall_dirs.txt
