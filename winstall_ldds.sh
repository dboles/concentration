#!/bin/bash

src=$1
dst=$2

dlls=$(ldd "$src" | cut -d' ' -f3 -s | grep -iv '^\(???\|/[a-z]/windows\)' | sort | uniq)

if [ -n "$dlls" ]
then
	cp -Lpt "$dst" $dlls
fi
