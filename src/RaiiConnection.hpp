#ifndef DJB_RAIICONNECTION_HPP
#define DJB_RAIICONNECTION_HPP

#include <sigc++/connection.h>

namespace djb {

struct RaiiConnection final {
	sigc::connection m_connection;

	explicit RaiiConnection(sigc::connection);
	~RaiiConnection();
};

} // namespace djb

#endif // DJB_RAIICONNECTION_HPP
