#ifndef DJB_ASSERT_PTR_HPP
#define DJB_ASSERT_PTR_HPP

#include <cassert>

namespace djb {

template <typename T>
class AssertPtr final {
public:
	AssertPtr() = default;

	explicit AssertPtr(T* const t2)
	: t{t2}
	{
		assert(t);
	}

	auto operator->() const
	{
		assert(t);
		return t;
	}

	auto& operator*() const
	{
		return *operator->();
	}

private:
	T* const t{};
};

} // namespace djb

#endif // DJB_ASSERTPTR_HPP
