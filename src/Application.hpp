#ifndef DJB_CONCENTRATION_APPLICATION_HPP
#define DJB_CONCENTRATION_APPLICATION_HPP

#include "model/Game.hpp"
#include "model/Scoreboard.hpp"

#include "gui/Window.hpp"

#include <gtkmm/application.h>

namespace djb::concentration {

class Application final: public Gtk::Application {
public:
	static auto create() -> Glib::RefPtr<Application>;

	Application   (Application const&) = delete;
	void operator=(Application const&) = delete;

private:
	model::Game m_game;
	model::Scoreboard m_scoreboard;

	gui::Window m_window;

	Application();

	virtual void on_startup() override final;
	virtual void on_activate() override final;

	void on_game_start ();
	void on_game_finish();

	void help();
	void quit();

	void add_action_with_accel(Glib::ustring, Glib::ustring const&,
	                           Gio::ActionMap::ActivateSlot const&);
};

} // namespace djb::concentration

#endif // DJB_CONCENTRATION_APPLICATION_HPP
