#ifndef DJB_CONCENTRATION_MODEL_CARD_HPP
#define DJB_CONCENTRATION_MODEL_CARD_HPP

#include <glibmm/object.h>
#include <glibmm/property.h>
#include <glibmm/ustring.h>

namespace djb::concentration::model {

class Card final: public Glib::Object {
public:
	Card(int value,
	     Glib::ustring const& symbol,
	     Glib::ustring const& front_image,
	     Glib::ustring const& front_description,
	     Glib::ustring const& back_image,
	     Glib::ustring const& back_description);

	auto property_value () const { return m_property_value .get_proxy(); }
	auto property_symbol() const { return m_property_symbol.get_proxy(); }

	auto property_front_image      () const { return m_property_front_image      .get_proxy(); }
	auto property_front_description() const { return m_property_front_description.get_proxy(); }

	auto property_back_image       () const { return m_property_back_image      .get_proxy(); }
	auto property_back_description () const { return m_property_back_description.get_proxy(); }

	enum class Face {back, front};
	auto property_face_up() const { return m_property_face_up.get_proxy(); }
	auto property_face_up()       { return m_property_face_up.get_proxy(); }

	auto property_flippable() const { return m_property_flippable.get_proxy(); }
	auto property_flippable()       { return m_property_flippable.get_proxy(); }

	enum class State {normal, matched, unmatched};
	auto property_state() const { return m_property_state.get_proxy(); }
	auto property_state()       { return m_property_state.get_proxy(); }

	void flip();

private:
	Glib::Property<int          > m_property_value {*this, "value" , {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};
	Glib::Property<Glib::ustring> m_property_symbol{*this, "symbol", {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};

	Glib::Property<Glib::ustring> m_property_front_image      {*this, "front-image"      , {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};
	Glib::Property<Glib::ustring> m_property_front_description{*this, "front-description", {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};

	Glib::Property<Glib::ustring> m_property_back_image       {*this, "back-image"       , {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};
	Glib::Property<Glib::ustring> m_property_back_description {*this, "back-description" , {}, {}, {}, Glib::PARAM_CONSTRUCT_ONLY};

	Glib::Property<Face > m_property_face_up  {*this, "face-up"  , Face::back   };
	Glib::Property<bool > m_property_flippable{*this, "flippable", true         };
	Glib::Property<State> m_property_state    {*this, "state"    , State::normal};
};

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_CARD_HPP
