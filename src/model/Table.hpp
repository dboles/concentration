#ifndef DJB_CONCENTRATION_MODEL_TABLE_HPP
#define DJB_CONCENTRATION_MODEL_TABLE_HPP

#include "Card.hpp"
#include "CardPtrs.hpp"
#include <sigc++/signal.h>

namespace sigc { class connection; }

namespace djb::concentration::model {

class Table final {
public:
	auto get_cards() const -> CardPtrs const&;

	void add_cards(CardPtrs);
	auto remove_cards() -> CardPtrs;

	auto on_added  (sigc::slot<void>) -> sigc::connection;
	auto on_removed(sigc::slot<void>) -> sigc::connection;

private:
	CardPtrs m_cards;

	sigc::signal<void> m_signal_added;
	sigc::signal<void> m_signal_removed;
};

} // namespace djb::concentation::model

#endif // DJB_CONCENTRATION_MODEL_TABLE_HPP
