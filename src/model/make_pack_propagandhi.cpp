#include "make_pack_propagandhi.hpp"

#include "algorithm.hpp"
#include <fmt/core.h>

#include <array>
#include <random>

namespace djb::concentration::model {

static void add_pair(Pack& pack, int const rand);

auto
make_pack_propagandhi() -> Pack
{
	static auto constexpr size = 16;
	auto pack = Pack{size};
	auto distribution = std::uniform_int_distribution<>{};
	auto random_engine = std::mt19937{ std::random_device{}() };
	auto rand = distribution(random_engine);
	do_times( size / 2, [&]{ add_pair(pack, rand++); } );
	return pack;
}

static void
add_pair(Pack& pack, int const rand)
{
	static auto constexpr f_symbols = std::array{"♠", "♥", "♦", "♣"};
	static auto constexpr f_names = std::array{"Chris", "Jord", "John", "Todd",
	                                           "Glen", "Beave", "Sulynn",
	                                           "rONALD", "ShMcSh", "Barney"};

	auto const   name_index = rand %   f_names.size();
	auto const symbol_index = rand % f_symbols.size();
	auto const symbol = Glib::ustring{ f_symbols.at(symbol_index) };
	auto const front_name = Glib::ustring{ f_names.at(name_index) };
	auto const front_tooltip = fmt::format( "Itʼs {}!", front_name.c_str() );

	do_times( 2, [&]
	{
		pack.add_card( std::make_unique<Card>(1 + name_index,
		                                      symbol,
		                                      front_name,
		                                      front_tooltip,
		                                      "shed",
		                                      "Whoʼs in the shed?") );
	} );
}

} // namespace djb::concentration::model
