#ifndef DJB_CONCENTRATION_MODEL_PACK_HPP
#define DJB_CONCENTRATION_MODEL_PACK_HPP

#include "Card.hpp"
#include "CardPtrs.hpp"

namespace djb::concentration::model {

class Pack final {
public:
	explicit Pack(int maximal_size = 52);

	void add_card(CardPtr);
	void add_cards(CardPtrs);

	void shuffle();

	// auto deal_card() -> CardPtr;
	auto deal_all() -> CardPtrs;
	// TODO: Add a deal_pair()?

private:
	int const m_maximal_size{};
	CardPtrs m_cards;
};

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_PACK_HPP
