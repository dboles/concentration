#ifndef DJB_CONCENTRATION_MODEL_SCOREBOARD_HPP
#define DJB_CONCENTRATION_MODEL_SCOREBOARD_HPP

#include "Score.hpp"

#include <glibmm/ustring.h>
#include <sigc++/signal.h>

#include <limits>
#include <optional>
#include <set>

namespace sigc { class connection; }

namespace djb::concentration::model {

class Scoreboard final {
public:
	using Scores = std::set<Score>;

	explicit Scoreboard( Glib::ustring filename,
	                     int maximal_size = std::numeric_limits<int>::max() );

	auto get_scores() const -> Scores const&;

	auto record_score(Score const& score) -> std::optional<int>;
	void save() const;
	void clear();

	auto on_changed(sigc::slot<void>) -> sigc::connection;

private:
	Glib::ustring const m_filename;
	int const m_maximal_size{};
	Scores m_scores;

	sigc::signal<void> m_signal_changed;

	void load();
	auto find_score_for(Glib::ustring const& name) -> std::optional<Scores::iterator>;
	auto replace_if_greater(Score const&, Scores::iterator) -> std::optional<int>;
	auto insert(Scores::iterator, Score const&) -> int;
};

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_SCOREBOARD_HPP
