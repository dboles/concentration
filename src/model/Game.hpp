#ifndef DJB_CONCENTRATION_MODEL_GAME_HPP
#define DJB_CONCENTRATION_MODEL_GAME_HPP

#include "model/Pack.hpp"
#include "model/Score.hpp"
#include "model/Table.hpp"

#include "RaiiConnection.hpp"

#include <glibmm/object.h>
#include <glibmm/property.h>
#include <sigc++/connection.h>

#include <functional>
#include <vector>

namespace Glib { class ustring; }

namespace djb::concentration::model {

class Game final: public Glib::Object {
public:
	Game(Pack, Glib::ustring player_name);

	Game          (Game const&) = delete;
	void operator=(Game const&) = delete;

	enum class State {stopped, playing, unmatched, matched, won, lost};
	auto property_state() const { return m_property_state.get_proxy(); }

	auto get_table() -> Table&;
	auto get_score() -> Score&;

	void start();
	void restart();

	auto on_start (sigc::slot<void>) -> sigc::connection;
	auto on_finish(sigc::slot<void>) -> sigc::connection;

private:
	Pack  m_pack;
	Table m_table;
	Score m_score;

	Glib::Property<State> m_property_state{*this, "state", State::stopped};

	sigc::signal<void> m_signal_start;
	sigc::signal<void> m_signal_finish;

	std::vector<RaiiConnection> m_connection_flips;
	std::vector< std::reference_wrapper<Card> > m_cards_flipped;
	int m_pairs_remaining{0};
	bool m_unflipping{false};

	void start(Card&);
	void resume_private();
	void set_state(State);

	void on_card_flipped(Card&);

	auto add_card_flipped(Card&) -> int;
	auto pair_match() const -> bool;
	void evaluate_pair();
	void evaluate_pair_matched();
	void evaluate_pair_unmatched();
	void set_pair_state(Card::State);
	void unflip_pair();

	void clear_table();
};

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_GAME_HPP
