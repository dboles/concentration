#ifndef DJB_CONCENTRATION_MODEL_SCORE_HPP
#define DJB_CONCENTRATION_MODEL_SCORE_HPP

#include <glibmm/ustring.h>
#include <sigc++/signal.h>
#include <compare>

namespace sigc { class connection; }

namespace djb::concentration::model {

class Score final
{
public:
	explicit Score(Glib::ustring name = {}, int points = 0);

	auto operator== (Score const&) const -> bool;
	auto operator<=>(Score const&) const -> std::weak_ordering;

	auto get_name() const -> Glib::ustring const&;
	void set_name(Glib::ustring);

	auto get_points() const -> int;
	void set_points(int);

	auto on_name_changed  (sigc::slot<void>) -> sigc::connection;
	auto on_points_changed(sigc::slot<void>) -> sigc::connection;

	static auto get_max_name_length() -> Glib::ustring::size_type;

private:
	Glib::ustring m_name;
	int           m_points{};

	sigc::signal<void> m_signal_name;
	sigc::signal<void> m_signal_points;
};

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_SCORE_HPP
