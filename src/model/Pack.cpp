#include "Pack.hpp"

#include "algorithm.hpp"

#include <algorithm>
#include <cassert>
#include <utility>

namespace djb::concentration::model {

Pack::Pack(int const maximal_size)
: m_maximal_size{maximal_size}
{
	assert(m_maximal_size > 0);

	m_cards.reserve(m_maximal_size);
}

void
Pack::add_card(CardPtr card)
{
	assert(std::ssize(m_cards) < m_maximal_size);

	m_cards.emplace_back( std::move(card) );
}

void
Pack::add_cards(CardPtrs cards)
{
	assert(std::ssize(m_cards) + std::ssize(cards) <= m_maximal_size);
	move_into(std::move(cards), m_cards);
}

#if 0
auto
Pack::deal_card() -> CardPtr
{
	assert( not m_cards.empty() );
	if    (     m_cards.empty() ) return nullptr;

	/* N.B. The guide says "Returns first card in list", but since the pack
	 * is shuffled, this works just as well and uses convenient syntax */
	auto card = std::move( m_cards.back() );
	m_cards.pop_back();
	return card;
}
#endif

auto
Pack::deal_all() -> CardPtrs
{
	return std::exchange( m_cards, {} );
}

void
Pack::shuffle()
{
	djb::random_shuffle(m_cards);
}

} // namespace djb::concentration::model
