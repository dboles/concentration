#include "Table.hpp"
#include <sigc++/connection.h>
#include <utility>

namespace djb::concentration::model {

void
Table::add_cards(CardPtrs cards)
{
	move_into(std::move(cards), m_cards);
	m_signal_added.emit();
}

auto
Table::get_cards() const -> CardPtrs const&
{
	return m_cards;
}

auto
Table::remove_cards() -> CardPtrs
{
	auto cards = std::exchange( m_cards, {} );
	m_signal_removed.emit();
	return cards;
}

auto
Table::on_added(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_added.connect( std::move(slot) );
}

auto
Table::on_removed(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_removed.connect( std::move(slot) );
}

} // namespace djb::concentation::model
