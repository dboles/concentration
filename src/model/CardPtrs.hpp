#ifndef DJB_CONCENTRATION_MODEL_CARDPTRS_HPP
#define DJB_CONCENTRATION_MODEL_CARDPTRS_HPP

#include <memory>
#include <vector>

namespace djb::concentration::model {

class Card;
using CardPtr = std::unique_ptr<Card>;
using CardPtrs = std::vector<CardPtr>;
void move_into(CardPtrs&& from, CardPtrs& to);

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_CARDPTRS_HPP
