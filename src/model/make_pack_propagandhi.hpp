#ifndef DJB_CONCENTRATION_MODEL_PACK_PROPAGANDHI_HPP
#define DJB_CONCENTRATION_MODEL_PACK_PROPAGANDHI_HPP

#include "Pack.hpp"

namespace djb::concentration::model {

auto make_pack_propagandhi() -> Pack;

} // namespace djb::concentration::model

#endif // DJB_CONCENTRATION_MODEL_PACK_PROPAGANDHI_HPP
