#include "Score.hpp"

#include <sigc++/connection.h>

#include <cassert>
#include <utility>
#include <tuple>

namespace djb::concentration::model {

Score::Score(Glib::ustring name, int const points)
:
	m_name{ std::move(name) },
	m_points{points}
{}


auto
Score::operator==(Score const& that) const -> bool
{
	return m_name   == that.m_name and
	       m_points == that.m_points;
}

auto
Score::operator<=>(Score const& that) const -> std::weak_ordering
{
	/* If same points, sort by name. As scores are shown with points in desc
	 * order, swap the names for comparison so those get shown in asc order.
	 * TODO: It would be nice to record the time at which each score was
	 * achieved and rank the players ascending by the earliest timestamp. */
	return std::tie(     m_points, that.m_name)
	   <=> std::tie(that.m_points,      m_name);
}


auto
Score::get_name() const -> Glib::ustring const&
{
	return m_name;
}

void
Score::set_name(Glib::ustring name)
{
	if (name == m_name) {
		return;
	}

	assert( name.size() <= get_max_name_length() );

	m_name = std::move(name);
	m_signal_name.emit();
}

auto
Score::get_points() const -> int
{
	return m_points;
}

void
Score::set_points(int const points)
{
	if (points == m_points) {
		return;
	}

	m_points = points;
	m_signal_points.emit();
}

auto
Score::on_name_changed(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_name.connect( std::move(slot) );
}

auto
Score::on_points_changed(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_points.connect( std::move(slot) );
}

auto
Score::get_max_name_length() -> Glib::ustring::size_type
{
	return 42;
}

} // namespace djb::concentration::model
