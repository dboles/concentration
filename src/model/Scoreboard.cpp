#include "Scoreboard.hpp"

#include "algorithm.hpp"

#include <fmt/core.h>
#include <glibmm/fileutils.h>
#include <glibmm/keyfile.h>
#include <sigc++/connection.h>

#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <stdexcept>
#include <utility>

namespace djb::concentration::model {

Scoreboard::Scoreboard(Glib::ustring filename, int const maximal_size)
:
	m_filename{ std::move(filename) },
	m_maximal_size{maximal_size}
{
	assert(m_maximal_size > 0);

	load();
}

auto
Scoreboard::get_scores() const -> Scores const&
{
	return m_scores;
}

auto
Scoreboard::record_score(Score const& score) -> std::optional<int>
{
	assert(std::ssize(m_scores) <= m_maximal_size);

	if ( auto const existing_score = find_score_for( score.get_name() ) ) {
		return replace_if_greater(score, *existing_score);
	}

	if (std::ssize(m_scores) < m_maximal_size) {
		return insert(m_scores.begin(), score);
	}

	return replace_if_greater( score, m_scores.begin() );
}

static auto
get_group_name(int const index)
{
	return Glib::ustring{ fmt::format("score{}", index) };
}

static auto
set_score(Glib::KeyFile& keyFile, int const index, Score const& score)
{
	auto const group_name = get_group_name(index);
	keyFile.set_string ( group_name, "name"  , score.get_name  () );
	keyFile.set_integer( group_name, "points", score.get_points() );
}

void
Scoreboard::save() const
try
{
	auto keyFile = Glib::KeyFile{};
	keyFile.set_integer( "count", "count", std::ssize(m_scores) );
	enumerate( m_scores, std::bind_front( &set_score, std::ref(keyFile) ) );
	auto ofstream = std::ofstream{m_filename};
	ofstream.exceptions(~std::ios_base::goodbit);
	ofstream << keyFile.to_data();
}
catch (Glib::Exception const& exception) {
	std::cerr << exception.what() << '\n';
	throw;
}
catch (std::exception const& exception) {
	std::cerr << exception.what() << '\n';
	throw;
}

void
Scoreboard::clear()
{
	m_scores.clear();
	m_signal_changed.emit();
}

auto
Scoreboard::on_changed(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_changed.connect( std::move(slot) );
}

static void
check_count(int const count, int const maximal_size)
{
	if (count < 0) {
		throw std::logic_error{"count must be 0 or greater!"};
	}

	if (count > maximal_size) {
		throw std::logic_error{"count exceeds maximal_size!"};
	}
}

static auto
get_score(Glib::KeyFile const& keyFile, int const index)
{
	auto const group_name = get_group_name(index);
	return Score{ keyFile.get_string (group_name, "name"  ),
	              keyFile.get_integer(group_name, "points") };
}

static auto
view_scores(Glib::KeyFile const& keyFile, int const count)
{
	auto const get1 = std::bind_front( get_score, std::ref(keyFile) );
	return std::views::iota(0, count) | std::views::transform(get1);
}

void
Scoreboard::load()
try
{
	auto keyFile = Glib::KeyFile{};
	keyFile.load_from_file(m_filename);

	auto const count = keyFile.get_integer("count", "count");
	check_count(count, m_maximal_size);

	auto const scores = view_scores(keyFile, count);
	m_scores = Scores{ scores.begin(), scores.end() };

	m_signal_changed.emit();
}
catch (Glib::Exception const& exception) {
	std::cerr << exception.what() << '\n';
	throw;
}
catch (std::exception const& exception) {
	std::cerr << exception.what() << '\n';
	throw;
}

auto
Scoreboard::find_score_for(Glib::ustring const& name) -> std::optional<Scores::iterator>
{
	auto const it = std::ranges::find(m_scores, name, &Score::get_name);
	return it != m_scores.end() ? std::optional{it} : std::nullopt;
}

auto
Scoreboard::replace_if_greater(Score const& score,
                               Scores::iterator const it) -> std::optional<int>
{
	if ( not (score > *it) ) {
		return std::nullopt;
	}

	auto const pos = m_scores.erase(it);
	return insert(pos, score);
}

auto
Scoreboard::insert(Scores::iterator const pos, Score const& score) -> int
{
	auto const old_size = m_scores.size();
	auto const it = m_scores.insert(pos, score);
	assert(m_scores.size() == old_size + 1);
	m_signal_changed.emit();
	return std::distance( it, m_scores.cend() );
}

} // namespace djb::concentration::model
