#include "Card.hpp"

namespace djb::concentration::model {

Card::Card(int const value,
           Glib::ustring const& symbol,
           Glib::ustring const& front_image,
           Glib::ustring const& front_description,
           Glib::ustring const& back_image,
           Glib::ustring const& back_description)
: Glib::ObjectBase{"model_Card"}
{
	m_property_value            .set_value(value            );
	m_property_symbol           .set_value(symbol           );
	m_property_front_image      .set_value(front_image      );
	m_property_front_description.set_value(front_description);
	m_property_back_image       .set_value(back_image       );
	m_property_back_description .set_value(back_description );
}

void
Card::flip()
{
	if (not m_property_flippable) {
		return;
	}

	m_property_face_up = m_property_face_up == Face::back ? Face::front : Face::back;
}

} // namespace djb::concentration::model
