#include "CardPtrs.hpp"
#include "Card.hpp"
#include <algorithm>
#include <iterator>
#include <utility>

namespace djb::concentration::model {

void
move_into(CardPtrs&& from, CardPtrs& to)
{
	if ( to.empty() ) {
		to = std::move(from);
	} else {
		to.reserve( to.size() + from.size() );
		std::ranges::move( from, std::back_inserter(to) );
	}
}

} // namespace djb::concentration::model
