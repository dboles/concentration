#include "Game.hpp"
#include "algorithm.hpp"

#include <algorithm>
#include <cassert>
#include <utility>

namespace djb::concentration::model {

Game::Game(Pack pack,
           Glib::ustring player_name)
:
	Glib::ObjectBase{"model_Game"},

	m_pack { std::move(pack       ) },
	m_score{ std::move(player_name) }
{}


auto
Game::get_table() -> Table&
{
	return m_table;
}

auto
Game::get_score() -> Score&
{
	return m_score;
}


auto
Game::on_start(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_start.connect( std::move(slot) );
}

auto
Game::on_finish(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_finish.connect( std::move(slot) );
}


void
Game::start(Card& card)
{
	card.property_flippable() = true;
	card.property_state() = Card::State::normal;
	// gui::Table.add_card() unflips to back, after undeal animation

	m_connection_flips.emplace_back(
		card.property_face_up().signal_changed().connect(
			[&]{ on_card_flipped(card); } ) );
}

void
Game::start()
{
	assert(m_property_state == State::stopped);

	// TODO: A real pack could have more cards than dealt. Add deal_pair()?
	m_pack.shuffle();
	auto cards = m_pack.deal_all();
	m_pairs_remaining = cards.size() / 2;
	m_connection_flips.reserve( cards.size() );
	std::ranges::for_each( cards, [this](auto const& card){ start(*card); } );
	m_table.add_cards( std::move(cards) );

	m_score.set_points(46);
	m_signal_start.emit();
	resume_private();
}

void
Game::resume_private()
{
	if (m_property_state == State::unmatched) {
		unflip_pair();
	}

	m_cards_flipped.clear();
	m_property_state = State::playing;
}

void
Game::restart()
{
	assert(m_property_state != State::stopped);

	m_property_state = State::stopped;
	clear_table();
	start();
}


void
Game::on_card_flipped(Card& card)
{
	if (m_unflipping) {
		return;
	}

	if (auto const state = m_property_state.get_value();
	    state == State::matched or state == State::unmatched)
	{
		auto const old_face_up = card.property_face_up().get_value();
		resume_private();

		if (card.property_face_up() != old_face_up) {
			return;
		}
	}

	// Ignore unflip by gui::Table.add_card()
	if (card.property_face_up() == Card::Face::back) {
	        return;
	}

	assert(m_property_state == State::playing);

	auto const points = m_score.get_points() - 1;
	m_score.set_points(points);

	if (add_card_flipped(card) == 2) {
		evaluate_pair();

		if (m_property_state == State::won) {
			return;
		}
	}

	if (points == 0) {
		m_property_state = State::lost;
		m_signal_finish.emit();
	}
}

auto
Game::add_card_flipped(Card& card) -> int
{
	assert(std::ranges::none_of(m_cards_flipped,
		[&](Card const& that){ return &that == &card; } ) );

	card.property_flippable() = false;
	m_cards_flipped.emplace_back(card);
	assert(m_cards_flipped.size() <= 2);
	return m_cards_flipped.size();
}

auto
Game::pair_match() const -> bool
{
	assert(m_cards_flipped.size() == 2);
	return all_equal(m_cards_flipped, &Card::property_value);
}

void
Game::evaluate_pair()
{
	assert(m_cards_flipped.size() == 2);

	if ( pair_match() ) {
		evaluate_pair_matched();
	} else {
		evaluate_pair_unmatched();
	}
}

void
Game::evaluate_pair_matched()
{
	set_pair_state(Card::State::matched);

	assert(m_pairs_remaining > 0);

	--m_pairs_remaining;
	auto const won = m_pairs_remaining == 0;
	m_property_state = won ? State::won : State::matched;

	if (won) {
		m_signal_finish.emit();
	}
}

void
Game::evaluate_pair_unmatched()
{
	set_pair_state(Card::State::unmatched);
	m_property_state = State::unmatched;

	for (Card& card: m_cards_flipped) {
		card.property_flippable() = true;
	}
}

void
Game::set_pair_state(Card::State const state)
{
	assert(m_cards_flipped.size() == 2);

	for (Card& card: m_cards_flipped) {
		card.property_state() = state;
	}
}

void
Game::unflip_pair()
{
	assert(m_cards_flipped.size() == 2);

	set_pair_state(Card::State::normal);
	m_unflipping = true;

	for (Card& card: m_cards_flipped) {
		assert( card.property_flippable() );
		card.flip();
	}

	m_unflipping = false;
}

void
Game::clear_table()
{
	m_connection_flips.clear();

	m_pack.add_cards( m_table.remove_cards() );
}

} // namespace djb::concentration::model
