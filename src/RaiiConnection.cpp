#include "RaiiConnection.hpp"
#include <utility>

namespace djb {

RaiiConnection::RaiiConnection(sigc::connection connection)
: m_connection{ std::move(connection) }
{}

RaiiConnection::~RaiiConnection()
{
	m_connection.disconnect();
}

} // namespace djb
