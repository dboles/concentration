#include "src/Application.hpp"

#include <glibmm/error.h>
#include <iostream>

auto
main() -> int
try
{
	auto const application = djb::concentration::Application::create();
	return application->run();
}
catch (Glib::Error const& error) {
	std::cerr << error.what() << std::endl;
	throw;
}
