#ifndef DJB_ASSERTED_CAST_HPP
#define DJB_ASSERTED_CAST_HPP

#include <cassert>
#include <memory>
#include <type_traits>

namespace djb {

template <typename T>
concept is_pointer = std::is_pointer_v<T>;

template <typename T>
concept is_reference = std::is_reference_v<T>;

template <is_pointer To>
auto
asserted_cast(is_pointer auto const from)
{
	assert(from != nullptr);
	assert( dynamic_cast<To>(from) );
	return   static_cast<To>(from);
}

template <is_reference To>
auto&
asserted_cast(is_pointer auto const from)
{
	using ToPtr = std::add_pointer_t<To>;
	return *asserted_cast<ToPtr>(from);
}

template <is_reference To>
auto&
asserted_cast(is_reference auto from)
{
	using ToRef = std::remove_reference_t<To>;
	return asserted_cast<ToRef>( std::addressof(from) );
}

} // namespace djb

#endif // DJB_ASSERTED_CAST_HPP
