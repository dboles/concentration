#include "Game.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include <glibmm/main.h>
#include <glibmm/ustring.h>
#include <gtkmm/label.h>
#include <gtkmm/overlay.h>
#include <gtkmm/stylecontext.h>

#include <array>
#include <tuple>

namespace djb::concentration::gui {

static auto constexpr c_type_name = "gui_Game";

void
Game::ensure_type()
{
	static_cast<void>( Game{} );
}

Game::Game()
: Glib::ObjectBase{c_type_name}
{}

Game::Game(BaseObjectType* const cobject, Glib::RefPtr<Gtk::Builder> const&,
           Model& model)
: Game{ cobject, model, create_builder<Score_OnGame, Table>("Game") }
{}

Game::Game(BaseObjectType* const cobject, Model& model,
           Glib::RefPtr<Gtk::Builder> const& builder)
:
	Glib::ObjectBase{c_type_name},
	Gtk::Bin{cobject},

	m_model{&model},
	m_old_state{ m_model->property_state() },

	m_label_message{ &get<Gtk::Label     &>(builder, "label_message") },
	m_gesturePan   {  get<Gtk::GesturePan >(builder, "gesturePan"   ) },

	m_score{ &get_derived<Score_OnGame&>( builder, "score", m_model->get_score() ) },
	m_table{ &get_derived<Table       &>( builder, "table", m_model->get_table() )  }
{
	add_style_class(*this, "Game");
	add( get<Gtk::Overlay&>(builder, "overlay") );

	m_model->on_start ( sigc::mem_fun(*this, &Game::on_model_start ) );
	m_model->on_finish( sigc::mem_fun(*this, &Game::on_model_finish) );
	m_model->property_state().signal_changed().connect ( sigc::mem_fun(*this, &Game::on_model_state ) );

	m_gesturePan->signal_pan().connect( sigc::mem_fun(*this, &Game::on_pan_table_vertical) );

	m_score->on_name_missing( sigc::mem_fun(*this, &Game::on_score_name_missing) );
}

void
Game::disconnect_score_focus_out()
{
	m_score->disconnect_focus_out();
}

namespace {

auto const&
get_stateInfo(Game::Model::State const state)
{
	using StateInfo = std::tuple<Glib::ustring, Glib::ustring, bool>;

	static auto const f_stateInfos = std::array<StateInfo, 6>{ {
		{ "stopped"  , {}                                     , {}    },
		{ "playing"  , "Letʼs play Concentration!"            , true  },
		{ "unmatched", "Thatʼs not a match!"                  , true  },
		{ "matched"  , "Congrats on the match!"               , true  },
		{ "won"      , "You just won the game!"               , false },
		{ "lost"     , "Sorry, your score hit 0, so you lose…", false },
	} };

	auto const index = static_cast<int>(state);
	return f_stateInfos.at(index);
}

} // namespace

void
Game::on_model_start()
{
	m_score->set_sensitive(true);

	on_score_name_missing();

	if ( m_score->get_name_missing() ) {
		m_score->prompt_for_name();
	}
}

void
Game::on_model_finish()
{
	// Don't allow play to continue, obviously
	m_table->set_sensitive(false);

	// The Score may already have been recorded, so don't let it be edited
	m_score->set_sensitive(false);
}

void
Game::on_model_state()
{
	auto const styleContext = get_style_context();

	auto const& old_stateInfo = get_stateInfo(m_old_state);
	styleContext->remove_class( std::get<0>(old_stateInfo) );

	auto const state = m_model->property_state().get_value();
	auto const& [css_class, message, is_temporary] = get_stateInfo(state);
	styleContext->add_class(css_class);

	if ( show_message(state) ) {
		set_message(message, is_temporary);
	} else {
		set_message( {} );
	}

	m_old_state = state;
}

auto
Game::show_message(Model::State const state) const -> bool
{
	// For State::playing, only show message on 1st turn; else clear message
	auto const was_stopped = m_old_state == Model::State::stopped;
	auto const now_playing =       state == Model::State::playing;
	auto const resuming = not was_stopped and now_playing;
	return not resuming;
}

void
Game::set_message(Glib::ustring const& message, bool const temporarily)
{
	m_connection_message.disconnect();

	auto const shown = not message.empty();
	toggle_style_class(*m_label_message, "shown", shown);

	if (not shown) {
		return;
	}

	m_label_message->set_text(message);

	if (not temporarily) {
		return;
	}

	m_connection_message = Glib::signal_timeout().connect( [this]
	{
		set_message( {} );
		return false;
	}, 16 * 333 );
}

void
Game::on_score_name_missing()
{
	m_table->set_sensitive( not m_score->get_name_missing() );

	// TODO: It'd be nice to block move to Scoreboard & grab cursor on Cards
}

void
Game::on_pan_table_vertical(Gtk::PanDirection const panDirection, double const offset)
{
	if (panDirection == Gtk::PAN_DIRECTION_DOWN and
	    offset >= get_allocated_height() / 4.0)
	{
		m_model->restart();
	}
}

} // namespace djb::concentration::gui
