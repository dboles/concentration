#ifndef DJB_CONCENTRATION_GUI_SCORE_ONGAME_HPP
#define DJB_CONCENTRATION_GUI_SCORE_ONGAME_HPP

#include "Score.hpp"
#include "AssertPtr.hpp"
#include <sigc++/signal.h>

namespace sigc { class connection; }
namespace Glib { template <typename> class RefPtr; }
namespace Gtk { class Builder; }

namespace djb::concentration::gui {

class Score_OnGame final: public Score {
public:
	static void ensure_type();

	Score_OnGame(BaseObjectType*, Glib::RefPtr<Gtk::Builder> const&,
	             Model&);

	Score_OnGame  (Score_OnGame const&) = delete;
	void operator=(Score_OnGame const&) = delete;

	auto get_name_missing() const -> bool;
	auto on_name_missing(sigc::slot<void>) -> sigc::connection;
	void prompt_for_name();
	void disconnect_focus_out();

private:
	AssertPtr<Model> const m_model;
	sigc::connection m_connection_entry_focus_out;
	sigc::signal<void> m_signal_name_missing;
	bool m_name_missing{};

	Score_OnGame();
	Score_OnGame(BaseObjectType*,
                     Model&, Glib::RefPtr<Gtk::Builder> const&);

	void on_entry_changed();
	auto on_entry_focus_out() -> bool;
};

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_SCORE_ONGAME_HPP
