#include "Score.hpp"

#include "builder.hpp"
#include "toggle_style_class.hpp"

#include "model/Score.hpp"

#include <fmt/format.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>

namespace djb::concentration::gui {

static auto&
get_entry_name(Glib::RefPtr<Gtk::Builder> const& builder)
{
	return get<Gtk::Entry&>(builder, "entry_name");
}

static auto&
get_label_points(Glib::RefPtr<Gtk::Builder> const& builder)
{
	return get<Gtk::Label&>(builder, "label_points");
}

Score::Score()
{}

Score::Score(Model const& model, Glib::RefPtr<Gtk::Builder> const& builder)
:
	m_entry_name  { &get_entry_name  (builder) },
	m_label_points{ &get_label_points(builder) }
{
	construct_impl(model);
}

Score::Score(BaseObjectType* const cobject, Model const& model,
             Glib::RefPtr<Gtk::Builder> const& builder)
:
	Gtk::Bin{cobject},

	m_entry_name  { &get_entry_name  (builder) },
	m_label_points{ &get_label_points(builder) }
{
	construct_impl(model);
}

void
Score::construct_impl(Model const& model)
{
	add_style_class(*this, "Score");

	on_model_name_changed  (model);
	on_model_points_changed(model);

	m_entry_name->set_max_length( Model::get_max_name_length() );
}

void
Score::on_model_name_changed(Model const& model)
{
	m_entry_name->set_text( model.get_name() );
}

void
Score::on_model_points_changed(Model const& model)
{
	m_label_points->set_text( fmt::to_string( model.get_points() ) );
}

} // namespace djb::concentration::gui
