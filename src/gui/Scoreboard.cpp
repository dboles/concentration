#include "Scoreboard.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include <fmt/core.h>
#include <glibmm/ustring.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/popover.h>
#include <gtkmm/sizegroup.h>

#include <utility>

namespace djb::concentration::gui {

static auto constexpr c_type_name = "gui_Scoreboard";

void
Scoreboard::ensure_type()
{
	static_cast<void>( Scoreboard{} );
}

Scoreboard::Scoreboard()
: Glib::ObjectBase{c_type_name}
{}

Scoreboard::Scoreboard(BaseObjectType* const cobject, Glib::RefPtr<Gtk::Builder> const&,
                       Model& model)
: Scoreboard{ cobject, model, create_builder("Scoreboard") }
{}

Scoreboard::Scoreboard(BaseObjectType* const cobject,
                       Model& model,
                       Glib::RefPtr<Gtk::Builder> const& builder)
:
	Glib::ObjectBase{c_type_name},
	Gtk::Bin{cobject},

	m_model{&model},

	m_button_clear  { &get<Gtk::Button &>(builder, "button_clear"  ) },
	m_label_congrats{ &get<Gtk::Label  &>(builder, "label_congrats") },
	m_box_list      { &get<Gtk::Box    &>(builder, "box_list"      ) },
	m_popover_clear { &get<Gtk::Popover&>(builder, "popover_clear" ) }
{
	add_style_class(*this, "Scoreboard");
	add( get<Gtk::Box&>(builder, "box_main") );

	m_button_clear->signal_clicked().connect( [this]{ prompt_clear(); } );

	get<Gtk::Button&>(builder, "button_clear_no" )
		.signal_clicked().connect( [this]{ answer_clear(false); } );

	get<Gtk::Button&>(builder, "button_clear_yes")
		.signal_clicked().connect( [this]{ answer_clear(true ); } );

	m_popover_clear->show_all_children();

	rebuild();
	m_model->on_changed( sigc::mem_fun(*this, &Scoreboard::rebuild) );
}

void
Scoreboard::highlight_rank(std::optional<int> const optional_rank)
{
	auto const highlighted = Glib::ustring{"highlighted"};

	for (auto& score: m_scores) {
		remove_style_class(score, highlighted);
	}

	auto const got_rank = optional_rank.has_value();
	m_label_congrats->set_visible(got_rank);

	if (not got_rank) {
		return;
	}

	auto const rank = *optional_rank;
	auto const index = rank - 1;
	add_style_class(m_scores.at(index), highlighted);
	m_label_congrats->set_text( fmt::format("Congratulations! You got rank #{}", rank) );
}

void
Scoreboard::rebuild()
{
	m_scores.clear();
	highlight_rank(std::nullopt);

	auto const& model_scores = m_model->get_scores();
	m_button_clear->set_sensitive( not model_scores.empty() );

	if ( model_scores.empty() ) {
		return;
	}

	m_scores.reserve( model_scores.size() );

	auto rank = 1;
	auto const sg_name   = Gtk::SizeGroup::create(Gtk::SIZE_GROUP_HORIZONTAL);
	auto const sg_points = Gtk::SizeGroup::create(Gtk::SIZE_GROUP_HORIZONTAL);

	for (auto const& model_score: model_scores) {
		auto& score = m_scores.emplace_back(model_score, rank++, sg_name, sg_points);
		score.show_all();
		m_box_list->add(score);
	}
}

void
Scoreboard::prompt_clear()
{
	m_popover_clear->popup();
}

void
Scoreboard::answer_clear(bool const clear)
{
	m_popover_clear->popdown();

	if (clear) {
		m_model->clear();
	}
}

} // namespace djb::concentration::gui
