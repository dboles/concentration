#include "Card.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include <fmt/format.h>
#include <glibmm/ustring.h>
#include <gdkmm/cursor.h>
#include <gdkmm/display.h>
#include <gdkmm/window.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/stack.h>

#include <cassert>

namespace djb::concentration::gui {

namespace {

// TODO: Ideally, images should scale intelligently relative to screen size.
// Load image to Pixbuf, scale based on screen size, and set that on image?
void
setup_image(Glib::RefPtr<Gtk::Builder> const& builder,
            Glib::UStringView const side,
            Glib::ustring const& filename,
            Glib::ustring const& description)
{
	auto& image = get<Gtk::Image&>( builder, fmt::format( "image_{}", side.c_str() ) );
	image.set_from_resource( fmt::format( "/org/djb/concentration/{}.png", filename.c_str() ) );
	image.set_tooltip_text ( fmt::format("({0}) {1}", side.c_str(), description.c_str() ) );
}

void
setup_symbol(Glib::RefPtr<Gtk::Builder> const& builder, char const* const align,
             Glib::ustring const& symbol, Glib::ustring const& value)
{
	auto const id = fmt::format("label_symbol_{}", align);
	auto& label = get<Gtk::Label&>(builder, id);
	label.set_label(symbol);
	label.set_tooltip_text(value);
}

void
setup_symbol(Card::Model const& model, Glib::RefPtr<Gtk::Builder> const& builder)
{
	auto const symbol = model.property_symbol().get_value();

	if ( symbol.empty() ) {
		return;
	}

	auto const value = Glib::ustring{ fmt::to_string( model.property_value() ) };
	setup_symbol(builder, "start", symbol, value);
	setup_symbol(builder, "end"  , symbol, value);
}

} // namespace

Card::Card(Model& model)
: Card{ model, create_builder("Card") }
{}

Card::Card(Model& model, Glib::RefPtr<Gtk::Builder> const& builder)
:
	Glib::ObjectBase{"gui_Card"},

	m_model{model},
	m_stack_main { get<Gtk::Stack&>(builder, "stack_main" ) },
	m_stack_state{ get<Gtk::Stack&>(builder, "stack_state") }
{
	add_style_class(*this, "Card");
	add(m_stack_main);

	setup_image( builder, "front", m_model.property_front_image(),
	                               m_model.property_front_description() );

	setup_image( builder, "back", m_model.property_back_image(),
	                              m_model.property_back_description() );

	setup_symbol(m_model, builder);

	m_stack_main.show_all();
	on_model_flip ();
	on_model_state();

	signal_clicked().connect( [this]{ flip(); } );

	signal_realize().connect( [this]{ on_model_flippable(); } );

	m_model.property_flippable().signal_changed().connect(
		sigc::mem_fun(*this, &Card::on_model_flippable) );

	m_model.property_face_up().signal_changed().connect(
		sigc::mem_fun(*this, &Card::on_model_flip) );

	m_model.property_state().signal_changed().connect(
		sigc::mem_fun(*this, &Card::on_model_state) );
}

void
Card::flip()
{
	if ( not m_model.property_flippable() ) {
		error_bell();
	} else {
		m_model.flip();
	}
}

void
Card::on_model_flip()
{
	auto const front_up = m_model.property_face_up() == Model::Face::front;
	toggle_style_class(*this, "back-up", "front-up", front_up);
	m_stack_main.set_visible_child(not front_up ? "back" : "front");
}

void
Card::on_model_flippable()
{
	auto const flippable = m_model.property_flippable().get_value();
	toggle_style_class(*this, "flippable", flippable);

	get_window()->set_cursor(not flippable ? Glib::RefPtr<Gdk::Cursor>{}
	                         : Gdk::Cursor::create(get_display(), "grab") );
}

void
Card::on_model_state()
{
	auto const state = m_model.property_state().get_value();
	auto const normal = state == Model::State::normal;
	toggle_style_class(m_stack_state, "shown", not normal);

	if (normal) {
		return;
	}

	auto const matched = state == Model::State::matched;
	toggle_style_class(*this, "unmatched", "matched", matched);
	m_stack_state.set_visible_child(not matched ? "unmatched" : "matched");
}

auto
Card::get_preferred_natural_size() const -> std::pair<int, int>
{
	assert( get_visible() );
	auto minimal = Gtk::Requisition{};
	auto natural = Gtk::Requisition{};
	get_preferred_size(minimal, natural);
	assert(natural.width  > 0);
	assert(natural.height > 0);
	return {natural.width, natural.height};
}

} // namespace djb::concentration::gui
