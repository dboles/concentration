#ifndef DJB_CONCENTRATION_GUI_WINDOW_HPP
#define DJB_CONCENTRATION_GUI_WINDOW_HPP

#include "Game.hpp"
#include "Scoreboard.hpp"

#include <gtkmm/applicationwindow.h>
#include <gtkmm/gesturepan.h>

#include <optional>

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Builder;
	class Button;
	class Stack;
}

namespace djb::concentration {

namespace model {
	class Game;
	class Scoreboard;
}

namespace gui {

class Window final: public Gtk::ApplicationWindow {
public:
	Window(model::Game&, model::Scoreboard&);
	~Window();

	Window        (Window const&) = delete;
	void operator=(Window const&) = delete;

	void show_game      ();
	void show_scoreboard();

	void highlight_rank( std::optional<int> );

private:
	Gtk::Button& m_button_restart;
	Gtk::Stack & m_stack;
	Glib::RefPtr<Gtk::GesturePan> const m_gesturePan;

	Game& m_game;
	Scoreboard& m_scoreboard;

	Window(model::Game&, model::Scoreboard&,
	       Glib::RefPtr<Gtk::Builder> const&);

	void on_game_start();
	void on_game_finish();
	void set_restart_is_destructive(bool);

	void on_pan_stack_horizontal(Gtk::PanDirection, double);
};

} // namespace gui

} // namespace djb::concentration

#endif // DJB_CONCENTRATION_GUI_WINDOW_HPP
