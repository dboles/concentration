#include "toggle_style_class.hpp"

#include <gtkmm/stylecontext.h>
#include <gtkmm/widget.h>

namespace djb::concentration::gui {

void
toggle_class(Glib::RefPtr<Gtk::StyleContext> const& styleContext,
             Glib::ustring const& name, bool const b)
{
	if (not b) {
		styleContext->remove_class(name);
	} else {
		styleContext->   add_class(name);
	}
}

void
toggle_class(Glib::RefPtr<Gtk::StyleContext> const& styleContext,
             Glib::ustring const& false_name,
             Glib::ustring const& true_name, bool const b)
{
	toggle_class(styleContext, false_name, not b);
	toggle_class(styleContext,  true_name,     b);
}

void
toggle_style_class(Gtk::Widget& widget,
                  Glib::ustring const& name, bool const b)
{
	toggle_class(widget.get_style_context(), name, b);
}

void
toggle_style_class(Gtk::Widget& widget,
                   Glib::ustring const& false_name,
                   Glib::ustring const& true_name, bool const b)
{
	toggle_class(widget.get_style_context(), false_name, true_name, b);
}

void
add_style_class(Gtk::Widget& widget, Glib::ustring const& name)
{
	toggle_style_class(widget, name, true);
}

void
remove_style_class(Gtk::Widget& widget, Glib::ustring const& name)
{
	toggle_style_class(widget, name, false);
}

} // namespace djb::concentration::gui
