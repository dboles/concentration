#ifndef DJB_CONCENTRATION_GUI_BUILDER_HPP
#define DJB_CONCENTRATION_GUI_BUILDER_HPP

#include <gtkmm/builder.h>

#include <cassert>
#include <concepts>
#include <type_traits>
#include <utility>

namespace djb {

template <typename T>
concept is_object = std::derived_from<T, Glib::Object>;

template <typename T>
concept is_widget = std::derived_from<T, Gtk::Widget>;

template <typename T_Pointer>
concept is_widget_pointer = std::is_pointer_v<T_Pointer> and
                            is_widget< std::remove_pointer_t<T_Pointer> >;

template <typename T_Reference>
concept is_widget_reference = std::is_reference_v<T_Reference> and
                              is_widget< std::remove_reference_t<T_Reference> >;

template <is_object T_Object>
auto
get_optional(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id)
{
	auto const base = builder->get_object(id);
	return Glib::RefPtr<T_Object>::cast_static(base);
}

template <is_object T_Object>
auto
get(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id)
{
	auto const object = get_optional<T_Object>(builder, id);
	assert( Glib::RefPtr<T_Object>::cast_dynamic(object) );
	return object;
}

template <is_widget_pointer T_Pointer>
auto
get(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id)
{
	auto pointer = T_Pointer{};
	builder->get_widget(id, pointer);
	return pointer;
}

template <is_widget_reference T_Reference>
auto&
get(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id)
{
	using Pointer = std::add_pointer_t<T_Reference>;
	auto const pointer = get<Pointer>(builder, id);
	assert(pointer);
	return *pointer;
}

template <is_widget_pointer T_Pointer, typename... T_Args>
auto
get_derived(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id,
            T_Args&&... args)
{
	auto pointer = T_Pointer{};
	builder->get_widget_derived(id, pointer, std::forward<T_Args>(args)...);
	return pointer;
}

template <is_widget_reference T_Reference, typename... T_Args>
auto&
get_derived(Glib::RefPtr<Gtk::Builder> const& builder, Glib::ustring const& id,
            T_Args&&... args)
{
	using Pointer = std::add_pointer_t<T_Reference>;
	auto const pointer = get_derived<Pointer>(builder, id, std::forward<T_Args>(args)...);
	assert(pointer);
	return *pointer;
}

} // namespace djb

#endif // DJB_CONCENTRATION_GUI_BUILDER_HPP
