#ifndef DJB_CONCENTRATION_GUI_SCOREBOARD_HPP
#define DJB_CONCENTRATION_GUI_SCOREBOARD_HPP

#include "Score_OnBoard.hpp"

#include "AssertPtr.hpp"
#include "model/Scoreboard.hpp"

#include <gtkmm/bin.h>

#include <optional>
#include <vector>

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Box;
	class Builder;
	class Button;
	class Label;
	class Popover;
}

namespace djb::concentration::gui {

/// Renders Scoreboard model as a grid of labels
class Scoreboard final: public Gtk::Bin {
public:
	using Model = model::Scoreboard;

	static void ensure_type();

	Scoreboard(BaseObjectType*, Glib::RefPtr<Gtk::Builder> const&, Model&);

	Scoreboard    (Scoreboard const&) = delete;
	void operator=(Scoreboard const&) = delete;

	void highlight_rank( std::optional<int> );

private:
	AssertPtr<Model> const m_model;

	std::vector<Score_OnBoard> m_scores;
	AssertPtr<Gtk::Button > const m_button_clear;
	AssertPtr<Gtk::Label  > const m_label_congrats;
	AssertPtr<Gtk::Box    > const m_box_list;
	AssertPtr<Gtk::Popover> const m_popover_clear;

	Scoreboard();
	Scoreboard(BaseObjectType*, Model&, Glib::RefPtr<Gtk::Builder> const&);

	void rebuild();

	void prompt_clear();
	void answer_clear(bool);
};

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_SCOREBOARD_HPP
