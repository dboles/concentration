#include "Window.hpp"

#include "model/Game.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include <glibmm/main.h>
#include <gtkmm/button.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/stack.h>

namespace djb::concentration::gui {

Window::Window(model::Game& model_game, model::Scoreboard& model_scoreboard)
: Window{ model_game, model_scoreboard,
          create_builder<Game, Scoreboard>("Window") }
{}

Window::Window(model::Game& model_game, model::Scoreboard& model_scoreboard,
               Glib::RefPtr<Gtk::Builder> const& builder)
:
	m_button_restart{ get<Gtk::Button    &>(builder, "button_restart") },
	m_stack         { get<Gtk::Stack     &>(builder, "stack"         ) },
	m_gesturePan    { get<Gtk::GesturePan >(builder, "gesturePan"    ) },

	m_game      { get_derived<Game      &>(builder, "game"      , model_game      ) },
	m_scoreboard{ get_derived<Scoreboard&>(builder, "scoreboard", model_scoreboard) }
{
	add_style_class(*this, "Concentration");
	set_resizable(false);
	set_titlebar( get<Gtk::HeaderBar&>(builder, "headerBar") );
	add(m_stack);

	m_gesturePan->signal_pan().connect(
		sigc::mem_fun(*this, &Window::on_pan_stack_horizontal) );

	// Pretend this is an image button using a ? icon
	remove_style_class(get<Gtk::Button&>(builder, "button_help"), "text-button");

	show_all_children();

	model_game.on_start ( sigc::mem_fun(*this, &Window::on_game_start ) );
	model_game.on_finish( sigc::mem_fun(*this, &Window::on_game_finish) );
}

Window::~Window()
{
	m_game.disconnect_score_focus_out();
}

void
Window::show_game()
{
	m_stack.set_visible_child("Game");
}

void
Window::show_scoreboard()
{
	m_stack.set_visible_child("Scoreboard");
}

void
Window::highlight_rank(std::optional<int> const optional_rank)
{
	m_scoreboard.highlight_rank(optional_rank);

	auto const got_rank = optional_rank.has_value();
	m_stack.child_property_needs_attention(
		*m_stack.get_child_by_name("Scoreboard") ) = got_rank;

	if (got_rank) {
		Glib::signal_timeout().connect_once(
			sigc::mem_fun(*this, &gui::Window::show_scoreboard), 4000);
	}
}

void
Window::on_game_start()
{
	show_game();
	set_restart_is_destructive(true);
}

void
Window::on_game_finish()
{
	set_restart_is_destructive(false);
}

void
Window::set_restart_is_destructive(bool const is_destructive)
{
	toggle_style_class(m_button_restart,
	                   GTK_STYLE_CLASS_SUGGESTED_ACTION,
	                   GTK_STYLE_CLASS_DESTRUCTIVE_ACTION, is_destructive);
}

void
Window::on_pan_stack_horizontal(Gtk::PanDirection const panDirection, double const offset)
{
	if ( not (offset >= m_stack.get_allocated_width() / 4.0) ) {
		return;
	}

	// Swipe the content right = Show the thing on the left
	if (panDirection == Gtk::PAN_DIRECTION_RIGHT) {
		show_game();
	} else {
		show_scoreboard();
	}
}

} // namespace djb::concentration::gui
