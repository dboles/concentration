#include "Table.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include "model/Table.hpp"
#include "algorithm.hpp"
#include "asserted_cast.hpp"

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <fmt/core.h>
#include <glibmm/main.h>
#include <gdkmm/screen.h>
#include <gtkmm/cssprovider.h>
#include <gtkmm/frame.h>
#include <gtkmm/overlay.h>
#include <gtkmm/stylecontext.h>
#include <sigc++/functors/mem_fun.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <optional>
#include <tuple>
#include <utility>

namespace djb::concentration::gui {

namespace {

auto constexpr c_n_rows = 4;
auto constexpr c_n_cols = 4;
auto constexpr c_deal_card_time_ms = 333;
auto constexpr c_undeal_all_time_ms = 4 * c_deal_card_time_ms;

auto s_card_natural_size = std::optional< std::pair<int, int> >{};

void
assert_dimension(int const pos, int const size)
{
	assert(pos >= 0   );
	assert(pos <  size);
}

void
assert_dimensions(int const row, int const col)
{
	assert_dimension(row, c_n_rows);
	assert_dimension(col, c_n_cols);
}

auto
get_overlay_sizes(int const card_width, int const card_height)
{
	auto const spacing = 1./27 * card_height;
	auto const padding = 1.5 * spacing;

	auto const lambda = [=](int const n_cards, int const card_size)
	{
		return 2 * padding + n_cards * card_size + (n_cards - 1) * spacing;
	};

	auto const width  = lambda(c_n_cols, card_width );
	auto const height = lambda(c_n_rows, card_height);
	return std::tuple{spacing, padding, width, height};
}

auto
get_card_position(int const index)
{
	auto const div = std::div(index, c_n_cols);
	auto const row = div.quot;
	auto const col = div.rem;
	assert_dimensions(row, col);
	return std::pair{row, col};
}

auto
get_css_properties(double const transition,
                   double const margin_left, double const margin_top)
{
	return fmt::format("transition: margin {}ms;"
	                   "margin-left: {}px;"
	                   "margin-top : {}px;",
		           transition, margin_left, margin_top);
}

auto
get_css(std::string const& css_class, int const row, int const col)
{
	auto const& [width, height] = s_card_natural_size.value();
	auto const [spacing, padding, total_width, total_height] = get_overlay_sizes(width, height);

	// Cards are dealt to their actual positions at a constant rate
	auto const dealt_x = padding + col * ( width + spacing);
	auto const dealt_y = padding + row * (height + spacing);

	// Undealt cards are in hidden row at bottom & go there in constant time
	auto const undealt_x = std::rand() % static_cast<int>(dealt_x / c_n_cols);
	auto const undealt_y = total_height;

	// c_deal… is max time, so × by (actual/max poss) dist for constant rate
	auto const distance = std::hypot(dealt_x - undealt_x, undealt_y - dealt_y);
	auto const ratio = distance / std::hypot(total_width, total_height);
	auto const deal_time_ms = ratio * c_deal_card_time_ms;

	auto const undealt_css = get_css_properties(c_undeal_all_time_ms, undealt_x, undealt_y);
	auto const   dealt_css = get_css_properties(        deal_time_ms,   dealt_x,   dealt_y);
	return fmt::format(".Card.{0}:not(.dealt) {{ {1} }}"
	                   ".Card.{0}.dealt {{ {2} }}",
	                   css_class, undealt_css, dealt_css);
}

void
add_at_position(Gtk::Overlay& overlay, Card& card, int const index)
{
	card.set_halign(Gtk::ALIGN_START);
	card.set_valign(Gtk::ALIGN_START);

	auto const css_class = fmt::format("i{}", index);
	card.get_style_context()->add_class(css_class);
	auto const [row, col] = get_card_position(index);
	auto const css = get_css(css_class, row, col);

	auto const cssProvider = Gtk::CssProvider::create();
	cssProvider->load_from_data(css);
	Gtk::StyleContext::add_provider_for_screen(Gdk::Screen::get_default(),
		cssProvider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

	overlay.add_overlay(card);
	overlay.set_overlay_pass_through(card, true);
}

} // namespace

static auto constexpr c_type_name = "gui_Table";

void
Table::ensure_type()
{
	static_cast<void>( Table{} );
}

Table::Table()
: Glib::ObjectBase{c_type_name}
{}

Table::Table(BaseObjectType* const cobject, Glib::RefPtr<Gtk::Builder> const&,
             Model& model)
: Table{ cobject, model, create_builder("Table") }
{}

Table::Table(BaseObjectType* const cobject, Model& model,
             Glib::RefPtr<Gtk::Builder> const& builder)
:
	Glib::ObjectBase{c_type_name},
	Gtk::Bin{cobject},

	m_model{&model},
	m_overlay{ &get<Gtk::Overlay&>(builder, "overlay") }
{
	add( get<Gtk::Frame&>(builder, "frame") );

	m_overlay->signal_key_press_event().connect(
		sigc::mem_fun(*this, &Table::on_overlay_key_press) );

	add_cards();
	m_model->on_added  ( sigc::mem_fun(*this, &Table::add_cards   ) );
	m_model->on_removed( sigc::mem_fun(*this, &Table::remove_cards) );
}

void
Table::add_card(model::Card& model_card)
{
	auto const n_children = m_cards.size();
	assert(n_children < c_n_rows * c_n_cols);

	// Unflip here as otherwise we do it too early, i.e. in undeal animation
	model_card.property_face_up() = model::Card::Face::back;

	auto& card = *m_cards.emplace_back( std::make_unique<Card>(model_card) );
	card.show();

	if (not s_card_natural_size) {
		s_card_natural_size = card.get_preferred_natural_size();
	}

	add_at_position(*m_overlay, card, n_children);
}

auto
Table::get_card(int const row, int const col) -> Card&
{
	assert_dimensions(row, col);
	auto const index = row * c_n_cols + col;
	return *m_cards[index];
}

auto
Table::get_focus_card() -> Card&
{
	return asserted_cast<Card&>( m_overlay->get_focus_child() );
}

auto
Table::get_position(Card const& card) const -> std::pair<int, int>
{
	auto const it = std::ranges::find(m_cards, &card, &CardPtr::get);
	assert( it != m_cards.end() );
	return get_card_position( it - m_cards.begin() );
}

void
Table::remove_cards()
{
	assert( not m_cards.empty() );

	if ( std::exchange(m_removing, true) ) {
		return;
	}

	m_connection_deals.clear();

	random_shuffled_for( m_cards, [this](CardPtr const& card)
	{
		// Randomise cardsʼ stacking/z-orders
		m_overlay->reorder_overlay(*card, 0);
		// Begin undeal animation for cards
		remove_style_class(*card, "dealt");
	} );

	// Give the CSS transitions time to run
	Glib::signal_timeout().connect_once(
		sigc::mem_fun(*this, &Table::remove_cards_2),
		c_undeal_all_time_ms);
}

void
Table::remove_cards_2()
{
	m_cards.clear();
	m_removing = false;

	if ( std::exchange(m_queued_add, false) ) {
		add_cards();
	}
}

void
Table::add_cards()
{
	if (m_removing) {
		m_queued_add = true;
		return;
	}

	auto const& model_cards = m_model->get_cards();

	if ( model_cards.empty() ) {
		return;
	}

	m_cards.reserve( model_cards.size() );
	std::ranges::for_each(model_cards, [this](auto const& card){ add_card(*card); } );

	/* Request enough size for the given number of rows of Cards,
	   _but_ hide low, dummy row of cards, which theyʼre all currently in */
	auto [width, height] = s_card_natural_size.value();
	std::tie(std::ignore, std::ignore, width, height) = get_overlay_sizes(width, height);
	m_overlay->set_size_request(width, height);

	deal();
}

void
Table::deal()
{
	assert( m_connection_deals.empty() );

	m_connection_deals.reserve( m_cards.size() );

	enumerate( m_cards, [this](int const i, CardPtr const& card)
	{
		m_connection_deals.emplace_back(
			Glib::signal_timeout().connect( [ card = card.get() ]
			{
				add_style_class(*card, "dealt");
				return false;
			}, (1 + i) * c_deal_card_time_ms) );
	} );
}

namespace {

auto
get_keynav_direction(GdkEventKey const* const eventKey)
-> std::optional< std::pair<int, int> >
{
	// Mirror GTK+ in moving focus if no modifier or only Ctrl
	if (eventKey->state & gtk_accelerator_get_default_mod_mask()
	    & ~GDK_CONTROL_MASK)
	{
		return std::nullopt;
	}

	switch (eventKey->keyval) {
		case GDK_KEY_Up:
		case GDK_KEY_KP_Up:
			return std::pair{-1,  0};

		case GDK_KEY_Down:
		case GDK_KEY_KP_Down:
			return std::pair{+1,  0};

		case GDK_KEY_Left:
		case GDK_KEY_KP_Left:
			return std::pair{ 0, -1};

		case GDK_KEY_Right:
		case GDK_KEY_KP_Right:
			return std::pair{ 0, +1};

		default:
			return std::nullopt;
	}
}

auto
wrap(int const pos, int const size, int const step)
{
	return ( pos + step + (step < 0 ? size : 0) ) % size;
}

} // namespace

// Hackery with Overlay+margins breaks GTK+ ::move-focus keynav, so do it myself
auto
Table::on_overlay_key_press(GdkEventKey const* const eventKey) -> bool
{
	auto const direction = get_keynav_direction(eventKey);

	if (direction == std::nullopt) {
		return false;
	}

	auto const& [row_step, col_step] = *direction;
	auto [row, col] = get_position( get_focus_card() );
	row = wrap(row, c_n_rows, row_step);
	col = wrap(col, c_n_cols, col_step);
	get_card(row, col).grab_focus();
	return true;
}

} // namespace djb::concentration::gui
