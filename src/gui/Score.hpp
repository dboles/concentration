#ifndef DJB_CONCENTRATION_GUI_SCORE_HPP
#define DJB_CONCENTRATION_GUI_SCORE_HPP

#include "AssertPtr.hpp"
#include <gtkmm/bin.h>

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Builder;
	class Entry;
	class Label;
}

namespace djb::concentration {

namespace model { class Score; }

namespace gui {

class Score: public Gtk::Bin {
public:
	using Model = model::Score;

protected:
	AssertPtr<Gtk::Entry> const m_entry_name;
	AssertPtr<Gtk::Label> const m_label_points;

	Score();
	Score(Model const&, Glib::RefPtr<Gtk::Builder> const&);
	Score(BaseObjectType*, Model const&, Glib::RefPtr<Gtk::Builder> const&);
	Score(Score&&) = default;
	~Score() = default;

	void on_model_name_changed  (Model const&);
	void on_model_points_changed(Model const&);

private:
	void construct_impl(Model const&);
};

} // namespace gui


} // namespace djb::concentration

#endif // DJB_CONCENTRATION_GUI_SCORE_HPP
