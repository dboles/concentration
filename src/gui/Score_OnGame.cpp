#include "Score_OnGame.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include "model/Score.hpp"

#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <sigc++/connection.h>

#include <utility>

namespace djb::concentration::gui {

static auto constexpr c_type_name = "gui_Score_OnGame";

void
Score_OnGame::ensure_type()
{
	static_cast<void>( Score_OnGame{} );
}

Score_OnGame::Score_OnGame()
: Glib::ObjectBase{c_type_name}
{}

Score_OnGame::Score_OnGame(BaseObjectType* const cobject,
                           Glib::RefPtr<Gtk::Builder> const&, Model& model)
: Score_OnGame{ cobject, model, create_builder("Score_OnGame") }
{}

Score_OnGame::Score_OnGame(BaseObjectType* const cobject, Model& model,
                           Glib::RefPtr<Gtk::Builder> const& builder)
:
	Glib::ObjectBase{c_type_name},
	Score{cobject, model, builder},

	m_model{&model},
	m_name_missing{ m_model->get_name().empty() }
{
	add_style_class(*this, "OnGame");
	add( get<Gtk::Box&>(builder, "box") );

	m_model->on_name_changed  ( [&]{ on_model_name_changed  (model); } );
	m_model->on_points_changed( [&]{ on_model_points_changed(model); } );

	m_entry_name->signal_changed().connect( [this]{ on_entry_changed(); } );

	m_connection_entry_focus_out = m_entry_name->signal_focus_out_event().connect(
		[this](GdkEventFocus const*){ return on_entry_focus_out(); } );
}

auto
Score_OnGame::get_name_missing() const -> bool
{
	return m_name_missing;
}

auto
Score_OnGame::on_name_missing(sigc::slot<void> slot) -> sigc::connection
{
	return m_signal_name_missing.connect( std::move(slot) );
}

void
Score_OnGame::prompt_for_name()
{
	m_entry_name->set_text( m_entry_name->get_placeholder_text() );
	m_entry_name->grab_focus();
}

void
Score_OnGame::disconnect_focus_out()
{
	m_connection_entry_focus_out.disconnect();
}

void
Score_OnGame::on_entry_changed()
{
	auto const text = m_entry_name->get_text();
	auto const name_missing = text.empty() or
	                          text == m_entry_name->get_placeholder_text();

	toggle_style_class(*m_entry_name, GTK_STYLE_CLASS_WARNING, name_missing);

	if (name_missing) {
		m_entry_name->set_icon_from_icon_name("dialog-question");
		m_entry_name->set_tooltip_text("Your name is required");
	} else {
		m_entry_name->set_icon_from_icon_name("user-info");
		m_entry_name->set_tooltip_text("Your name");

		m_model->set_name(text);
	}

	if (name_missing == m_name_missing) {
		return;
	}

	m_name_missing = name_missing;
	m_signal_name_missing.emit();
}

auto
Score_OnGame::on_entry_focus_out() -> bool
{
	if (not m_name_missing) {
		return false;
	}

	if ( not m_entry_name->is_focus() ) {
		m_entry_name->error_bell();
	}

	prompt_for_name();
	return true;
}

} // namespace djb::concentration::gui
