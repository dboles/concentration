#include "Score_OnBoard.hpp"

#include "builder.hpp"
#include "create_builder.hpp"
#include "toggle_style_class.hpp"

#include "model/Score.hpp"

#include <fmt/format.h>
#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/label.h>
#include <gtkmm/sizegroup.h>

namespace djb::concentration::gui {

Score_OnBoard::Score_OnBoard(Model const& model, int const rank,
                             Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_name,
                             Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_points)
: Score_OnBoard{ model, rank, sizeGroup_name, sizeGroup_points,
                 create_builder("Score_OnBoard") }
{}

Score_OnBoard::Score_OnBoard(Model const& model, int const rank,
                             Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_name,
                             Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_points,
                             Glib::RefPtr<Gtk::Builder  > const& builder)
: Score{model, builder}
{
	add_style_class(*this, "OnBoard");
	add( get<Gtk::Box&>(builder, "box") );

	auto& label_rank = get<Gtk::Label&>(builder, "label_rank");
	label_rank.set_label( fmt::to_string(rank) );

	sizeGroup_name  ->add_widget(*m_entry_name  );
	sizeGroup_points->add_widget(*m_label_points);
}

} // namespace djb::concentration::gui
