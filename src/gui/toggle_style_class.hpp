#ifndef DJB_CONCENTRATION_GUI_TOGGLE_STYLE_CLASS_HPP
#define DJB_CONCENTRATION_GUI_TOGGLE_STYLE_CLASS_HPP

namespace Glib {
	class ustring;
	template <typename> class RefPtr;
}

namespace Gtk {
	class StyleContext;
	class Widget;
}

namespace djb::concentration::gui {

void toggle_class(Glib::RefPtr<Gtk::StyleContext> const&,
                  Glib::ustring const& name, bool);

void toggle_class(Glib::RefPtr<Gtk::StyleContext> const&,
                  Glib::ustring const& false_name,
                  Glib::ustring const& true_name, bool);

void toggle_style_class(Gtk::Widget&,
                        Glib::ustring const& name, bool);

void toggle_style_class(Gtk::Widget&,
                        Glib::ustring const& false_name,
                        Glib::ustring const&  true_name, bool);

void    add_style_class(Gtk::Widget&, Glib::ustring const& name);
void remove_style_class(Gtk::Widget&, Glib::ustring const& name);

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_TOGGLE_STYLE_CLASS_HPP
