#include "create_builder.hpp"
#include <fmt/core.h>
#include <glibmm/ustring.h>

namespace djb::concentration::gui::detail {

static auto
get_resource_path(char const* const type)
{
	return fmt::format("/org/djb/concentration/{}.ui", type);
}

auto
create_builder(char const* const type) -> Glib::RefPtr<Gtk::Builder>
{
	return Gtk::Builder::create_from_resource( get_resource_path(type) );
}

} // namespace djb::concentration::gui::detail
