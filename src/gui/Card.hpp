#ifndef DJB_CONCENTRATION_GUI_CARD_HPP
#define DJB_CONCENTRATION_GUI_CARD_HPP

#include "model/Card.hpp"
#include <gtkmm/button.h>
#include <utility>

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Builder;
	class Stack;
}

namespace djb::concentration::gui {

class Card final: public Gtk::Button {
public:
	using Model = model::Card;

	explicit Card(Model&);

	Card          (Card const&) = delete;
	void operator=(Card const&) = delete;

	auto get_preferred_natural_size() const -> std::pair<int, int>;

private:
	Model& m_model;

	Gtk::Stack& m_stack_main;
	Gtk::Stack& m_stack_state;

	Card(Model&, Glib::RefPtr<Gtk::Builder> const&);

	void flip();

	void on_model_flip     ();
	void on_model_flippable();
	void on_model_state    ();
};

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_CARD_HPP
