#ifndef DJB_CONCENTRATION_GUI_GAME_HPP
#define DJB_CONCENTRATION_GUI_GAME_HPP

#include "Score_OnGame.hpp"
#include "Table.hpp"

#include "AssertPtr.hpp"
#include "model/Game.hpp"

#include <gtkmm/bin.h>
#include <gtkmm/gesturepan.h>
#include <sigc++/connection.h>

namespace Glib {
	class ustring;
	template <typename> class RefPtr;
}

namespace Gtk { class Builder; }

namespace djb::concentration::gui {

/// Class representing an active game: its score and playing area
class Game final: public Gtk::Bin {
public:
	using Model = model::Game;

	static void ensure_type();

	Game(BaseObjectType*, Glib::RefPtr<Gtk::Builder> const&,
	     Model&);

	Game          (Game const&) = delete;
	void operator=(Game const&) = delete;

	void disconnect_score_focus_out();

private:
	AssertPtr<Model> const m_model;
	Model::State m_old_state{};

	AssertPtr<Gtk::Label> const m_label_message;
	Glib::RefPtr<Gtk::GesturePan> const m_gesturePan;

	AssertPtr<Score_OnGame> const m_score;
	AssertPtr<Table> const m_table;

	sigc::connection m_connection_message;

	Game();
	Game(BaseObjectType*, Model&, Glib::RefPtr<Gtk::Builder> const&);

	void on_model_start ();
	void on_model_finish();
	void on_model_state ();

	auto show_message(Model::State) const -> bool;
	void set_message(Glib::ustring const&, bool temporarily = false);

	void on_score_name_missing();
	void on_pan_table_vertical(Gtk::PanDirection, double);
};

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_GAME_HPP
