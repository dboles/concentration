#ifndef DJB_CONCENTRATION_GUI_TABLE_HPP
#define DJB_CONCENTRATION_GUI_TABLE_HPP

#include "Card.hpp"

#include "AssertPtr.hpp"
#include "RaiiConnection.hpp"

#include <gtkmm/bin.h>

#include <memory>
#include <utility>
#include <vector>

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Builder;
	class Overlay;
}

namespace djb::concentration {

namespace model { class Table; }

namespace gui {

class Table final: public Gtk::Bin {
public:
	using Model = model::Table;

	static void ensure_type();

	Table(BaseObjectType*, Glib::RefPtr<Gtk::Builder> const&,
	      Model&);

	Table         (Table const&) = delete;
	void operator=(Table const&) = delete;

	void deal();

private:
	using CardPtr = std::unique_ptr<Card>;
	using CardPtrs = std::vector<CardPtr>;

	AssertPtr<Model> const m_model;
	CardPtrs m_cards;
	std::vector<RaiiConnection> m_connection_deals;
	bool m_removing{};
	bool m_queued_add{};

	AssertPtr<Gtk::Overlay> const m_overlay;

	Table();
	Table(BaseObjectType*, Model&, Glib::RefPtr<Gtk::Builder> const&);

	void add_card(model::Card&);
	auto get_card(int row, int col) -> Card&;
	auto get_focus_card() -> Card&;
	auto get_position(Card const&) const -> std::pair<int, int>;

	void remove_cards();
	void remove_cards_2();
	void add_cards();

	auto on_overlay_key_press(GdkEventKey const*) -> bool;
};

} // namespace gui

} // namespace djb::concentration

#endif // DJB_CONCENTRATION_GUI_TABLE_HPP
