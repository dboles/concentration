#ifndef DJB_CONCENTRATION_GUI_CREATE_BUILDER_HPP
#define DJB_CONCENTRATION_GUI_CREATE_BUILDER_HPP

#include <gtkmm/builder.h>

namespace djb::concentration::gui {

namespace detail { auto create_builder(char const*) -> Glib::RefPtr<Gtk::Builder>; }

template <typename... EnsureTypes>
auto
create_builder(char const* type)
{
	(EnsureTypes::ensure_type(), ...);
	return detail::create_builder(type);
}

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_CREATE_BUILDER_HPP
