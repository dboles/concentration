#ifndef DJB_CONCENTRATION_GUI_SCORE_ONBOARD_HPP
#define DJB_CONCENTRATION_GUI_SCORE_ONBOARD_HPP

#include "Score.hpp"

namespace Glib { template <typename> class RefPtr; }

namespace Gtk {
	class Builder;
	class SizeGroup;
}

namespace djb::concentration::gui {

class Score_OnBoard final: public Score {
public:
	Score_OnBoard(Model const&, int rank,
	              Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_name,
	              Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_points);

private:
	Score_OnBoard(Model const&, int rank,
	              Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_name,
	              Glib::RefPtr<Gtk::SizeGroup> const& sizeGroup_points,
	              Glib::RefPtr<Gtk::Builder  > const&);
};

} // namespace djb::concentration::gui

#endif // DJB_CONCENTRATION_GUI_SCORE_ONBOARD_HPP
