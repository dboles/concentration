#ifndef DJB_ALGORITHM_HPP
#define DJB_ALGORITHM_HPP

#include <algorithm>
#include <functional>
#include <random>
#include <ranges>
#include <type_traits>
#include <vector>

namespace djb {

template <typename Proj = std::identity>
auto
all_equal( std::ranges::range auto const& range, Proj proj = {} )
{
	return std::ranges::end(range) == std::ranges::adjacent_find(
		range, std::ranges::not_equal_to{}, std::move(proj) );
}

void
do_times(int times, auto&& func)
{
	while (times--) {
		func();
	}
}

void
enumerate(std::ranges::range auto&& range, auto&& func)
{
	for (auto i = 0; auto&& elem: range) {
		func( i++, std::forward<decltype(elem)>(elem) );
	}
}

template <std::ranges::range Range>
auto
make_refs(Range& range)
{
	using Reference = std::ranges::range_reference_t<Range>;
	using EffectiveValue = std::remove_reference_t<Reference>;
	using Ref = std::reference_wrapper<EffectiveValue>;
	return std::vector<Ref>{ std::ranges::begin(range),
	                         std::ranges::end  (range) };
}

void
random_shuffle(std::ranges::range auto& range)
{
	std::ranges::shuffle( range, std::mt19937{ std::random_device{}() } );
}

void
random_shuffled_for(std::ranges::range auto&& range, auto&& func)
{
	auto refs = make_refs(range);
	djb::random_shuffle(refs);
	auto const proj = [](auto const ref) -> auto& { return ref.get(); };
	std::ranges::for_each(refs, func, proj);
}

} // namespace djb

#endif // DJB_ALGORITHM_HPP
