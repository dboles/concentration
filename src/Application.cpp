#include <config.h>

#include "Application.hpp"

extern "C" {
#include "concentration.gresource.h"
}

#include "model/make_pack_propagandhi.hpp"

#include <fmt/core.h>
#include <glibmm/miscutils.h>
#include <gtk/gtk.h>
#include <gtkmm/cssprovider.h>

namespace djb::concentration {

namespace {

auto
get_default_player_name()
{
	if ( auto real_name = Glib::get_real_name();
	     not (real_name.empty() or real_name == "Unknown") )
	{
		return real_name;
	}

	return Glib::get_user_name();
}

} // namespace

Application::Application()
:
	Gtk::Application{ (concentration_register_resource(), // hack?
	                   "org.djb.concentration") },

	m_game{ model::make_pack_propagandhi(),
	        get_default_player_name() },

	m_scoreboard{DJB_SCOREBOARD_DIR "/scoreboard.txt", 10},

	m_window{m_game, m_scoreboard}
{
	auto const cssProvider = Gtk::CssProvider::create();
	cssProvider->load_from_resource("/org/djb/concentration/style.css");
	Gtk::StyleContext::add_provider_for_screen(
		Gdk::Screen::get_default(), cssProvider,
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

auto
Application::create() -> Glib::RefPtr<Application>
{
	return Glib::RefPtr<Application>{new Application};
}

void
Application::on_startup()
{
	Gtk::Application::on_startup();

	add_action_with_accel( "game"      , "<primary>g", [this]{ m_window.show_game();       } );
	add_action_with_accel( "scoreboard", "<primary>s", [this]{ m_window.show_scoreboard(); } );
	add_action_with_accel( "restart"   , "<primary>r", [this]{ m_game.restart();           } );
	add_action_with_accel( "help"      , "F1"        , [this]{ help();                     } );
	add_action_with_accel( "quit"      , "<primary>q", [this]{ quit();                     } );

	// Cancel out https://bugzilla.gnome.org/show_bug.cgi?id=766457
	set_accel_for_action("win.show-help-overlay", "<primary>question");

	m_window.signal_delete_event().connect( [this](GdkEventAny const*)
	{
		quit();
		return true;
	} );
	add_window(m_window);

	m_game.on_start ( sigc::mem_fun(*this, &Application::on_game_start ) );
	m_game.on_finish( sigc::mem_fun(*this, &Application::on_game_finish) );

	m_game.start();
}

void
Application::on_activate()
{
	Gtk::Application::on_activate();

	m_window.present();
}

void
Application::on_game_start()
{
	m_window.highlight_rank(std::nullopt);
}

void
Application::on_game_finish()
{
	if (m_game.property_state() != model::Game::State::won) {
		return;
	}

	auto const score = m_game.get_score();
	auto const rank = m_scoreboard.record_score(score);
	m_window.highlight_rank(rank);
}

void
Application::help()
{
	auto path = std::string{DJB_MANUAL_DIR};

	if (path.at(0) == '.') {
		path.replace( 0, 1, Glib::get_current_dir() );
	}

	path = fmt::format("file:///{}/manual.html", path);

	// N.B. This needs gspawn-win64-helper.exe in the same directory
	gtk_show_uri_on_window(GTK_WINDOW( m_window.gobj() ), path.c_str(),
	                       GDK_CURRENT_TIME, NULL);
}

void
Application::quit()
{
	m_scoreboard.save();
	std::ranges::for_each(get_windows(), &Gtk::Widget::hide);
}

void
Application::add_action_with_accel(Glib::ustring name, Glib::ustring const& accel,
                                   Gio::ActionMap::ActivateSlot const& slot)
{
	add_action(name, slot);
	set_accel_for_action(name.insert(0, "app."), accel);
}

} // namespace djb::concentration
