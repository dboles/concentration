#include "src/model/Card.hpp"

#include "tests/assert.hpp"

#include <glibmm/init.h>

auto
main() -> int
{
	using namespace djb::concentration::model;

	Glib::init();

	auto card = Card{42, "!", "test", "A test card", "test", "back of a test card"};

	// initial state
	DJB_ASSERT(card.property_face_up() == Card::Face::back)

	// Test flipping
	card.flip();
	DJB_ASSERT(card.property_face_up() == Card::Face::front)

	// Test flipping back
	card.flip();
	DJB_ASSERT(card.property_face_up() == Card::Face::back)

	return 0;
}
