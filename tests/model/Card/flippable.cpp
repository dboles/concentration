#include "src/model/Card.hpp"

#include "tests/assert.hpp"

#include <glibmm/init.h>

auto
main() -> int
{
	using namespace djb::concentration::model;

	Glib::init();

	auto card = Card{42, "!", "test", "A test card", "test", "back of a test card"};

	// initial state
	DJB_ASSERT(card.property_face_up() == Card::Face::back)
	DJB_ASSERT(card.property_flippable() == true)

	// Test flipping succeeds when flippable
	card.flip();
	DJB_ASSERT(card.property_face_up() == Card::Face::front)

	// Test changing flippability
	card.property_flippable() = false;
	DJB_ASSERT(card.property_flippable() == false)

	// Test flipping fails when not flippable
	card.flip();
	DJB_ASSERT(card.property_face_up() == Card::Face::front)

	return 0;
}
