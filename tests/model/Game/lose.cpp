#include "src/model/Game.hpp"

#include "tests/assert.hpp"

auto
main() -> int
{
	using namespace djb::concentration::model;

	auto game = Game{};
	game.start();
	DJB_ASSERT(game.get_state() == Game::State::playing)
	game.get_score().set_points(0);
	DJB_ASSERT(game.get_state() == Game::State::lost)

	return 0;
}
