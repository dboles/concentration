#include "src/model/Game.hpp"
#include "src/model/make_pack_propagandhi.hpp"

#include "tests/assert.hpp"

#include <glibmm/init.h>

#include <algorithm>
#include <concepts>
#include <cstdlib>

using namespace djb::concentration::model;

static auto
to_bool(char const* const c_str)
{
	return std::atoi(c_str) != 0;
}

static auto
get_expected_state(bool const flip_match, bool const last_point)
{
	return not last_point ? flip_match ? Game::State::matched : Game::State::unmatched
	                      : flip_match ? Game::State::won     : Game::State::lost;
}

static auto&
get_ref_if(CardPtrs const& cards, std::invocable<Card const&> auto const& pred)
{
	auto const proj = [](auto& cardPtr) -> auto& { return *cardPtr.get(); };
	auto const it = std::ranges::find_if(cards, pred, proj);
	DJB_ASSERT( it != cards.end() )
	return *( it->get() );
}

static auto&
find_other(CardPtrs const& cards, Card const& card_1, bool const flip_match)
{
	return get_ref_if(cards, [&](Card const& that)
	{
		return &that != &card_1 and
		       flip_match == ( that.property_value() == card_1.property_value() );
	} );
}

static auto&
find_other(CardPtrs const& cards, Card const& card_1, Card const& card_2)
{
	return get_ref_if(cards, [&](Card const& that)
	{
		return &that != &card_1 and &that != &card_2;
	} );
}

static void
test_new_game_is_stopped(Game const& game)
{
	DJB_ASSERT(game.property_state() == Game::State::stopped)
}

static void
test_started_game_is_playing(Game& game)
{
	game.start();
	DJB_ASSERT(game.property_state() == Game::State::playing)
}

static auto const&
test_flip_card_still_playing(Game& game)
{
	auto& card = *game.get_table().get_cards().front();
	card.flip();
	DJB_ASSERT(game.property_state() == Game::State::playing)
	return card;
}

static auto&
test_flipping_2nd(bool const flip_match, bool const last_point, Game& game, Card const& card_1)
{
	if (last_point) {
		game.get_score().set_points(1);
	}

	auto const& cards = game.get_table().get_cards();
	auto& card_2 = find_other(cards, card_1, flip_match);
	card_2.flip();
	auto const expected_state = get_expected_state(flip_match, last_point);
	DJB_ASSERT(game.property_state() == expected_state)
	return card_2;
}

static auto
get_flipping_same_expected_state(Game::State const state)
{
	switch (state) {
		case Game::State::matched:
			return Game::State::matched;

		case Game::State::unmatched:
			return Game::State::playing;

		default:
			DJB_ASSERT(false and "unhandled State");
	}
}

static void
test_flipping_3rd_playing(bool const last_point,
                          Game& game, Card const& card_1, Card& card_2)
{
	if (last_point) {
		return;
	}

	auto const old_state = game.property_state().get_value();
	auto const new_state = get_flipping_same_expected_state(old_state);
	card_2.flip();
	DJB_ASSERT(card_2.property_face_up() == Card::Face::front)
	DJB_ASSERT(game.property_state() == new_state);

	// FIXME: needs a test for flipping 3rd OTHER card after being unmatched

	if (new_state == Game::State::matched) {
		find_other(game.get_table().get_cards(), card_1, card_2).flip();
		DJB_ASSERT(game.property_state() == Game::State::playing)
	}
}

auto
main(int argc, char** argv) -> int
{
	DJB_ASSERT(argc == 3)

	Glib::init();

	auto const flip_match = to_bool( argv[1] );
	auto const last_point = to_bool( argv[2] );

	auto game = Game{make_pack_propagandhi(), "test_model_Game_state"};
	test_new_game_is_stopped(game);
	test_started_game_is_playing(game);
	auto const& card_1 = test_flip_card_still_playing(game);
	auto      & card_2 = test_flipping_2nd(flip_match, last_point, game, card_1);
	test_flipping_3rd_playing(last_point, game, card_1, card_2);

	return 0;
}
