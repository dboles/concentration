#include "src/model/Score.hpp"
#include "src/model/Scoreboard.hpp"

#include "tests/assert.hpp"

auto
main(int const argc, char** const argv) -> int
{
	using namespace djb::concentration::model;

	DJB_ASSERT(argc == 2)

	auto constexpr c_max_size = 5;

	try {
		auto scoreboard = Scoreboard{argv[1], c_max_size};
		auto const original_scores = scoreboard.get_scores();
		auto const n_scores = std::ssize( scoreboard.get_scores() );
		DJB_ASSERT(n_scores == c_max_size)

		// Adding a tied score should not be recorded
		scoreboard.record_score( Score{"NonWinner1", 2} );
		DJB_ASSERT(std::ssize( scoreboard.get_scores() ) == c_max_size)

		// Nor should a lower score
		scoreboard.record_score( Score{"NonWinner1", 1} );
		DJB_ASSERT(std::ssize( scoreboard.get_scores() ) == c_max_size)

		// TODO: This should be in its own cpp
		// Beating an existing score should update it
		auto const rank = scoreboard.record_score( Score{"Francis", 43} );
		DJB_ASSERT(rank == 1)
		DJB_ASSERT(std::ssize( scoreboard.get_scores() ) == c_max_size)

		// Check that a difference was made
		auto const scores = scoreboard.get_scores();
		DJB_ASSERT(scores != original_scores)
	}
	catch (...) {
		DJB_ASSERT(not "exception");
	}

	return 0;
}
