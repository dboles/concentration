#include "src/model/Score.hpp"
#include "src/model/Scoreboard.hpp"

#include "algorithm.hpp"
#include "tests/assert.hpp"

#include <fmt/core.h>
#include <iterator>

using namespace djb::concentration::model;

static auto constexpr c_limit = 10;

static auto
make_scoreboard(Glib::ustring const& filename)
{
	auto scoreboard = Scoreboard{filename, c_limit};
	DJB_ASSERT( not scoreboard.get_scores().empty() );
	DJB_ASSERT(std::ssize( scoreboard.get_scores() ) < c_limit);
	return scoreboard;
}

static void
fill(Scoreboard& scoreboard)
{
	for (auto points = scoreboard.get_scores().crbegin()->get_points();
	     scoreboard.record_score( Score{fmt::format("TEST{}", points), points} );
	     --points);
	DJB_ASSERT( std::ssize( scoreboard.get_scores() ) == c_limit)
}

static void
test_cannot_add_lower_score(Scoreboard& scoreboard)
{
	auto const min_points = scoreboard.get_scores().cbegin()->get_points();
	auto const old_scores = scoreboard.get_scores();
	scoreboard.record_score( Score{"TEST", min_points - 1 } );
	DJB_ASSERT(scoreboard.get_scores() == old_scores)
}

static void
test_higher_for_other_player(Scoreboard& scoreboard)
{
	auto const min_score = *scoreboard.get_scores().cbegin();
	auto const new_score = Score{"NEWTEST", min_score.get_points() + 1};
	scoreboard.record_score(new_score);
	DJB_ASSERT( not scoreboard.get_scores().contains(min_score) )
	DJB_ASSERT(     scoreboard.get_scores().contains(new_score) )
}

static void
test_higher_for_same_player(Scoreboard& scoreboard)
{
	auto const cbegin = scoreboard.get_scores().cbegin();
	auto const min_score = *cbegin;
	auto const old_score = *std::next(cbegin);
	auto const new_score = Score{old_score.get_name(), old_score.get_points() + 1};
	scoreboard.record_score(new_score);
	DJB_ASSERT(     scoreboard.get_scores().contains(min_score) )
	DJB_ASSERT( not scoreboard.get_scores().contains(old_score) )
	DJB_ASSERT(     scoreboard.get_scores().contains(new_score) )
}

auto
main(int const argc, char** const argv) -> int
try
{
	DJB_ASSERT(argc == 2)
	auto scoreboard = make_scoreboard( argv[1] );
	fill(scoreboard);
	test_cannot_add_lower_score (scoreboard);
	test_higher_for_other_player(scoreboard);
	test_higher_for_same_player (scoreboard);
	return 0;
}
catch (...) {
	DJB_ASSERT(not "exception");
}
