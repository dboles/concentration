#include "src/model/Scoreboard.hpp"

#include "tests/assert.hpp"

auto
main(int const argc, char** const argv) -> int
{
	using namespace djb::concentration::model;

	DJB_ASSERT(argc == 2)

	try {
		auto const scoreboard = Scoreboard{ argv[1] };
		auto const scores = scoreboard.get_scores();
		scoreboard.save();

		// Reloading from the same file should produce the same scores
		auto const scoreboard_b = Scoreboard{ argv[1] };
		DJB_ASSERT(scoreboard_b.get_scores() == scores);
	}
	catch (...) {
		DJB_ASSERT(not "exception");
	}

	return 0;
}
