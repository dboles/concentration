#!/bin/sh

tmp_txt=tmp-$DJB_MESON_TEST_SCOREBOARD_ACTION.txt

cp "$DJB_MESON_TEST_SCOREBOARD_TXT" $tmp_txt

"$DJB_MESON_TEST_SCOREBOARD_EXE" $tmp_txt
result=$?

if test $result -eq 0
then
	rm -f $tmp_txt
fi

exit $result
