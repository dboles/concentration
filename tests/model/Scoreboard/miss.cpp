#include "src/model/Score.hpp"
#include "src/model/Scoreboard.hpp"

#include "tests/assert.hpp"

auto
main(int const argc, char** const argv) -> int
{
	using namespace djb::concentration::model;

	DJB_ASSERT(argc == 2)

	try {
		auto scoreboard = Scoreboard{ argv[1] };
		auto const original_scores = scoreboard.get_scores();

		// Adding the same score should not be recorded
		auto score = *std::prev( original_scores.cend() );
		scoreboard.record_score(score);
		DJB_ASSERT(scoreboard.get_scores() == original_scores)

		// Nor should adding a lower score
		score.set_points(score.get_points() - 1);
		scoreboard.record_score(score);
		DJB_ASSERT(scoreboard.get_scores() == original_scores)

		// Nor should adding the lowest possible score
		score.set_points(0);
		scoreboard.record_score(score);
		DJB_ASSERT(scoreboard.get_scores() == original_scores)
	}
	catch (...) {
		DJB_ASSERT(not "exception");
	}

	return 0;
}
