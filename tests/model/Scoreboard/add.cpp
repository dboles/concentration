#include "src/model/Score.hpp"
#include "src/model/Scoreboard.hpp"

#include "tests/assert.hpp"

auto
main(int const argc, char** const argv) -> int
{
	using namespace djb::concentration::model;

	DJB_ASSERT(argc == 2)

	try {
		// Create a Scoreboard with no limit on its size
		auto scoreboard = Scoreboard{ argv[1] };
		auto const size_1 = std::ssize( scoreboard.get_scores() );

		// Adding a score with no limit should return the correct rank
		auto rank = scoreboard.record_score( Score{"TEST", 41} );
		DJB_ASSERT(rank == 2)

		// and increase the count of scores by 1
		auto const size_2 = std::ssize( scoreboard.get_scores() );
		DJB_ASSERT(size_1 == size_2 - 1)

		// Beating an existing score should update it
		rank = scoreboard.record_score( Score{"Francis", 43} );
		DJB_ASSERT(rank == 1)

		// which means the size does not change
		auto const size_3 = std::ssize( scoreboard.get_scores() );
		DJB_ASSERT(size_2 == size_3)
	}
	catch (...) {
		DJB_ASSERT(not "exception");
	}

	return 0;
}
