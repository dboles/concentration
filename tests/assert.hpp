#ifndef DJB_ASSERT_HPP
#define DJB_ASSERT_HPP

#include <cstdio>
#include <cstdlib>

namespace djb::assert {

static auto s_test_num = 0;

} // namespace djb::assert

#define DJB_ASSERT(cond)                                                                 \
	++::djb::assert::s_test_num;                                                     \
	                                                                                 \
	if ( not (cond) ) {                                                                 \
		std::printf("%s:%i - assertion failed - %s", __FILE__, __LINE__, #cond); \
		std::exit(::djb::assert::s_test_num);                                    \
	}

#endif // DJB_ASSERT_HPP
